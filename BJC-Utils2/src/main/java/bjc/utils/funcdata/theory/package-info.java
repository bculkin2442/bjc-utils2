/**
 * Random functional type things that don't belong elsewhere
 *
 * @author ben
 *
 */
package bjc.utils.funcdata.theory;